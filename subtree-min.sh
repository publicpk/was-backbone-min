#!/bin/bash

function subtree-repo() {
    git remote show backbone_remote
    if [ $? -eq 0 ]; then
        echo ">>> REMOTE exists."
        git checkout backbone_branch
        git pull --no-tags
    else
        # git remote add -f --no-tags backbone_remote https://github.com/backbone/mean.git
        git remote add backbone_remote git@publicpk.bitbucket.org:publicpk/was-backbone.git
        git fetch backbone_remote --no-tags
        git checkout -b backbone_branch backbone_remote/master
    fi
    git checkout master
}

function subtree-merge() {
  git merge --squash -s recursive backbone_branch
}

subtree-repo
subtree-merge
