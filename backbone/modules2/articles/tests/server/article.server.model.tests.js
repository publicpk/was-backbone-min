'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  path = require('path'),
  mongoose = require('mongoose');

/**
 * Globals
 */
var user,
  User,
  Article,
  article;

/**
 * Unit tests
 */
describe('Article Model Unit Tests:', function () {
  var mymongoose = require(path.resolve('./config/lib/mongoose'));

  before(function (done) {
    mymongoose.connect(function () {
      mymongoose.loadModels();
      User = mongoose.model('User');
      Article = mongoose.model('Article');
      return done();
    });
  });

  after(function(done) {
    mymongoose.disconnect(function () {
      return done();
    });
  });

  beforeEach(function (done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    });

    user.save(function () {
      article = new Article({
        title: 'Article Title',
        content: 'Article Content',
        user: user
      });

      return done();
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Article.remove().exec(function() {
        return done();
      });
    });
  });

  describe('Method Save', function () {
    it('should be able to save without problems', function (done) {
      this.timeout(10000);
      article.save(function (err) {
        should.not.exist(err);
        return done();
      });
    });

    it('should be able to show an error when try to save without title', function (done) {
      article.title = '';

      article.save(function (err) {
        should.exist(err);
        return done();
      });
    });
  });

});
