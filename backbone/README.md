# Backbone source

## Test
- Unit test
```
npm run test:server
npm run test:client
npm run test:e2e
```

- Test coverage
```
npm run test:coverage
```

- nyc test using mocha(https://istanbul.js.org/)
```
NODE_ENV=test nyc --reporter=html --reporter=text mocha modules/*/tests/server/**/*.js
```
