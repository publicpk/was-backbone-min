#!/bin/bash

# Usage
# ./script.sh (development | production) [init | build | run | stop | rm | ps | shell | build | gen]

NGINX__WAS_IP=${NGINX__WAS_IP:-"172.21.5.3"}
NGINX__NETWORK_NAME=${NGINX__NETWORK_NAME:-"nginx-network"}

MYWAS_NETWORK_NAME="my-was-network"

WEB_CONTAINER_NAME="web-server"
WEB_VOLUME_NAME="web-data"
DB_CONTAINER_NAME="db-server"
DB_VOLUME_NAME="db-data"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd ${DIR}/..

if [ "$1" == "production" ] ; then
    COMPOSE_FILE="docker-compose-production.yml"
    NODE_ENV="production"
    shift
else
    COMPOSE_FILE="docker-compose.yml"
    NODE_ENV="development"
    if [ "$1" == "development" ] ; then
        shift
    fi
fi

if [ ! -e "$COMPOSE_FILE" ]; then
    echo "Compose file does NOT exist. $COMPOSE_FILE"
    popd
    exit 1
fi

CMD=${1:-build}

echo "##############################"
echo ">>> $COMPOSE_FILE $CMD"

export MYWAS_NETWORK_NAME NGINX__NETWORK_NAME NGINX__WAS_IP

if [ "$CMD" == "init" ]; then
    docker volume inspect ${WEB_VOLUME_NAME} --format "{{.Name}}"
    [ "$?" != 0 ] && docker volume create ${WEB_VOLUME_NAME}
    docker volume inspect ${DB_VOLUME_NAME} --format "{{.Name}}"
    [ "$?" != 0 ] && docker volume create ${DB_VOLUME_NAME}

    docker network inspect ${MYWAS_NETWORK_NAME} --format "{{.Name}}"
    [ "$?" != 0 ] && docker network create ${MYWAS_NETWORK_NAME}
elif [ "$CMD" == "build" ]; then
    docker-compose -f ${COMPOSE_FILE} build
elif [ "$CMD" == "run" ]; then
    docker network inspect ${NGINX__NETWORK_NAME} --format "{{.Name}}"
    [ "$?" != 0 ] && echo "Run nginx container script to create nginx-network..." && exit 1

    docker-compose -f ${COMPOSE_FILE} up -d
elif [ "$CMD" == "stop" ] || [ "$CMD" == "down" ]; then
    docker-compose -f ${COMPOSE_FILE} down
elif [ "$CMD" == "rm" ]; then
    docker-compose -f ${COMPOSE_FILE} rm -f
elif [ "$CMD" == "ps" ]; then
    docker ps -a
elif [ "$CMD" == "info" ]; then
    IP=$(docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}, {{end}}' ${WEB_CONTAINER_NAME})
    echo "$WEB_CONTAINER_NAME IP Address: $IP"
elif [ "$CMD" == "shell" ]; then
    docker exec -t -i ${WEB_CONTAINER_NAME} /bin/bash
elif [ "$CMD" == "gen" ]; then
    # Generate Dockerfile
    echo ">>> Generate $COMPOSE_FILE..."
     cat > ${COMPOSE_FILE} <<EOF
 # Auto-generated file. DO NOT edit
version: '3'
services:
  web:
    restart: always
    build: .
    image: ${WEB_CONTAINER_NAME}:1.0.0
    container_name: web-server
    ports:
      - "3000:3000"
      - "5858:5858"
      - "8080:8080"
      - "35729:35729"
    environment:
      - NODE_ENV=${NODE_ENV}
      - DB_1_PORT_27017_TCP_ADDR=db
    depends_on:
      - db
    volumes:
      - web-data:/opt/my-was
    networks:
      front:
        ipv4_address: ${NGINX__WAS_IP:-172.21.5.3}
      backend:

  db:
    image: mongo:3.2
    container_name: ${DB_CONTAINER_NAME}
    restart: always
    ports:
      - "27017:27017"
    volumes:
      - db-data:/data/db
    networks:
      - backend

volumes:
  web-data:
    external:
      name: ${WEB_VOLUME_NAME}
  db-data:
    external:
      name: ${DB_VOLUME_NAME}

networks:
  front:
    external:
      name: ${NGINX__NETWORK_NAME:-"nginx-network"}
  backend:
    external:
      name: ${MYWAS_NETWORK_NAME:-"my-was-network"}

EOF
fi

popd
